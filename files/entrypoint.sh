#!/bin/sh
set -e

# Checks for USER variable
if [ -z "$USER" ]; then
  echo >&2 'Please set an USER variable (ie.: -e USER=john).'
  exit 1
fi

# Checks for PASSWORD variable
if [ -z "$PASSWORD" ]; then
  echo >&2 'Please set a PASSWORD variable (ie.: -e PASSWORD=hackme).'
  exit 1
fi

if /usr/bin/id -u ${USER}; then
  echo "User ${USER} already exists"
else
  echo "Creating user ${USER} with home /data"
  adduser -D -h /data ${USER}
  echo "${USER}:${PASSWORD}" | chpasswd
fi

if [ ! -d /data/results ]; then
  echo "Creating /data/tone"
  mkdir -p /data/results
  mkdir -p /data/offline
  mkdir -p /data/wslogo
fi

# The folder itself must be owned by root, the contents
# by the user
echo "Fixing permissions for user ${USER} in /data/tone"
chown -Rv ${USER}:${USER} /data/results
chown -Rv ${USER}:${USER} /data/offline
chown -Rv ${USER}:${USER} /data/wslogo
chmod -Rv 644 /data/results
chmod -Rv 644 /data/offline
chmod -Rv 644 /data/wslogo
chown root.root /data/results
chown root.root /data/offline
chmod 777 /data/results
chmod 777 /data/offline
chmod 777 /data/wslogo

echo "Fixing permission to root in /data"
chown root.root /data
chmod 755 /data

# Generate unique ssh keys for this container, if needed
if [ ! -f /etc/ssh/keys/ssh_host_ed25519_key ]; then
    ssh-keygen -t ed25519 -f /etc/ssh/keys/ssh_host_ed25519_key -N ''
fi
if [ ! -f /etc/ssh/keys/ssh_host_rsa_key ]; then
    ssh-keygen -t rsa -b 4096 -f /etc/ssh/keys/ssh_host_rsa_key -N ''
fi

exec /usr/bin/supervisord -n -c /etc/supervisord.conf
