FROM alpine:3.6

# Install packages
RUN sed -i 's/dl-cdn.alpinelinux.org/mirrors.aliyun.com/g' /etc/apk/repositories
RUN apk add --no-cache nginx supervisor openssh-server openssh-sftp-server

# NGINX
RUN mkdir -p /run/nginx/ && \
    ln -sf /dev/stdout /var/log/nginx/access.log && \
    ln -sf /dev/stderr /var/log/nginx/error.log

ADD files/nginx.conf /etc/nginx/conf.d/default.conf

# SSH/SFTP
ADD files/sshd_config /etc/ssh/
ADD files/index.html /data/index.html

# Supervisord
ADD files/supervisord.ini /etc/supervisor.d/
ADD files/docker_kill.py files/entrypoint.sh /

# Configuration for Container
VOLUME /data /etc/ssh/keys/
EXPOSE 22 80

# Creates users, checks permissions, generates host-keys and launches supervisord
CMD ["/entrypoint.sh"]
